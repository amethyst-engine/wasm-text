# Instructions

1. Make sure you have `cargo-web` installed. You can use the command `cargo install cargo-web`.
2. Run `cargo web start`
3. Go to `http://localhost:8000`